<!-- Steps calculator created by Erik Edgren, https://airikr.me -->
<?php

	function svgicon($string) {
		$arr = [
			'footprints' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="240" height="240"><path fill="none" d="M0 0h24v24H0z"/><path d="M4 18h5.5v1.25a2.75 2.75 0 1 1-5.5 0V18zM8 6.12c2 0 3 2.88 3 4.88 0 1-.5 2-1 3.5L9.5 16H4c0-1-.5-2.5-.5-5S5.498 6.12 8 6.12zm12.054 7.978l-.217 1.231a2.75 2.75 0 0 1-5.417-.955l.218-1.23 5.416.954zM18.178 1.705c2.464.434 4.018 3.124 3.584 5.586-.434 2.463-1.187 3.853-1.36 4.838l-5.417-.955-.232-1.564c-.232-1.564-.55-2.636-.377-3.62.347-1.97 1.832-4.632 3.802-4.285z"/></svg>',
			'git' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M7.105 15.21A3.001 3.001 0 1 1 5 15.17V8.83a3.001 3.001 0 1 1 2 0V12c.836-.628 1.874-1 3-1h4a3.001 3.001 0 0 0 2.895-2.21 3.001 3.001 0 1 1 2.032.064A5.001 5.001 0 0 1 14 13h-4a3.001 3.001 0 0 0-2.895 2.21z"/></svg>'
		];

		return $arr[$string];
	}

?><!DOCTYPE html>
<html>
	<head>
		<title>Calculate steps</title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="robots" content="noindex, nofollow">
		<meta name="google" content="notranslate">

		<link href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQEAYAAABPYyMiAAAABmJLR0T///////8JWPfcAAAACXBIWXMAAABIAAAASABGyWs+AAAAF0lEQVRIx2NgGAWjYBSMglEwCkbBSAcACBAAAeaR9cIAAAAASUVORK5CYII=" rel="icon" type="image/x-icon">

		<link type="text/css" rel="stylesheet" href="https://cdn.airikr.me/fonts.php?arr=inter:400,victor+mono:400">
		<style type="text/css">
			html, body {
				background-color: #0f0f0f;
				color: #c3c4c5;
				font-family: 'Inter', sans-serif;
				font-size: 0.960rem;
				line-height: 1.400rem;
				margin: 0;
				padding: 0;
			}

			body {
				overflow-y: scroll;
			}

			a {
				color: #c5def2;
				text-decoration: underline;
			}

			p {
				margin: 0;
				padding: 5px 0;
				text-align: justify;
			}

			code {
				font-family: 'Victor Mono', monospace;
				font-size: 100%;
			}

			input {
				background-color: transparent;
				border: 0;
				border-bottom: 1px solid #373737;
				color: #c3c4c5;
				font-family: 'Victor Mono', sans-serif;
				font-size: 100%;
				font-weight: 400;
				outline: none;
				padding: 5px;
			}

			main {
				margin: 0 auto;
				padding: 30px;
				max-width: 600px;
			}

			main > noscript > div {
				color: #7fb7eb;
				margin-top: -20px;
				padding: 20px;
			}

			main > noscript > div > a {
				color: #7fb7eb;
			}

			main > noscript > div > hr {
				border: 0;
				border-top: 1px solid #373737;
				margin: 40px auto 20px auto;
				width: 80%;
			}

			main > header {
				align-items: center;
				display: flex;
				margin-bottom: 20px;
				margin-left: 10px;
				flex-direction: row;
			}

			main > header > h2 {
				font-size: 200%;
				font-weight: 400;
				line-height: 100%;
				padding: 0;
			}

			main > header > svg {
				display: block;
				height: 64px;
				margin-right: 20px;
				line-height: 100%;
				width: 64px;

				-webkit-transform: rotate(45deg);
				-moz-transform: rotate(45deg);
				-ms-transform: rotate(45deg);
				-o-transform: rotate(45deg);
				transform: rotate(45deg);
			}

			main > header > svg > path:last-child {
				fill: #999999;
			}

			main > div#content {
				margin-bottom: 20px;
			}

			main > [name="km"] {
				margin-right: 20px;
				width: 150px;
			}

			main > [name="stepdistance"] {
				width: calc(100% - 190px);
			}

			main > div#result,
 			main > div#error,
 			main > div#notice {
				font-family: 'Victor Mono', monospace;
				font-size: 100%;
				margin-top: 10px;
			}

			main > div#error {
				color: #f1aeae;
			}

			main > div#notice {
				color: #aed7f1;
			}

			main > div#sourcecode {
				align-items: center;
				display: flex;
				margin-top: 30px;
				font-size: 90%;
				flex-direction: row;
			}

			main > div#sourcecode > svg {
				height: 20px;
				margin-right: 10px;
				width: 20px;
			}

			main > div#sourcecode > svg > path:last-child {
				fill: #999999;
			}



			@media only screen and (max-width: 660px) {
				p {
					text-align: left;
				}
			}
		</style>


		<script type="text/javascript" src="https://cdn.airikr.me/jquery.min.js"></script>
		<script type="text/javascript" src="https://cdn.airikr.me/validator.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {

				$('input').keyup(function() {
					var kilometers = $('[name="km"]').val().replace(',', '.');
					var stepdistance = $('[name="stepdistance"]').val().replace(',', '.');
					var stepdistance_zero = (stepdistance.length == 2 ? stepdistance + '0' : stepdistance);
					var kilometers_span = $('.km');
					var stepdistance_span = $('.stepdistance');
					var steps = $('.steps');
					var stepdistance_result = (stepdistance.length >= 2 ? stepdistance_zero.replace(/(\d{1})(\d{2,})/, "0.$1$2") : stepdistance_zero);
					var result = ((kilometers * 1) * 1000) / stepdistance_result;


					if(validator.isEmpty(kilometers) || validator.isEmpty(stepdistance)) {
						$('div#result').show();
						$('div#error').hide();
						$('div#notice').hide();

					} else {
						if(!validator.isNumeric(kilometers)) {
							$('div#result').hide();
							$('div#error').show().text('Error: Kilometers must contain a number (e.g. 3) or a delimiter (e.g. 3.46)');

						} else if(!validator.isNumeric(stepdistance, {no_symbols: true})) {
							$('div#result').hide();
							$('div#error').show().text('Error: Step distance contains illegal characters');

						} else if(stepdistance.length < 2) {
							$('div#result').hide();
							$('div#notice').show().text('Notice: step distance value must be 2 digits long');


						} else {
							$('div#result').show();
							$('div#error').hide();
							$('div#notice').hide();

							kilometers_span.text(kilometers);
							stepdistance_span.text(stepdistance_result);
							steps.text(parseFloat(result.toFixed(0)));
						}
					}

					if(validator.isEmpty(kilometers)) { kilometers_span.text('km');steps.text('0'); }
					if(validator.isEmpty(stepdistance)) { stepdistance_span.text('steps');steps.text('0'); }
				});

			});
		</script>
	</head>
	<body>



		<main>

			<noscript><div>
				<p><b>You need JavaScript enabled to use this website</b> since that solution is way better and simpler than using PHP.</p>
				<hr>
			</div></noscript>

			<?php
				echo '<header>';
					echo svgicon('footprints');
					echo '<h2>Steps calculator<h2>';
				echo '</header>';


				echo '<div id="content">';
					echo '<p>Use the instructed text fields below to calculate how many steps you have taken based on walked kilometers. The <code>~</code> sign for the result means approximately since you can take different length of the steps while walked.</p>';
					echo '<p>To measure your step distance, take a measurement tool while you\'re walking around like you normally do. Stop when both feet are touching the floor/ground and measure the distance between your heels or your toes.</p>';
				echo '</div>';

				echo '<input type="text" name="km" placeholder="Kilometer" tabindex="1" autocomplete="off">';
				echo '<input type="text" name="stepdistance" placeholder="Step distance in cm (65.0 cm = 650)" tabindex="2" autocomplete="off">';

				echo '<div id="error"></div>';
				echo '<div id="notice"></div>';

				echo '<div id="result">';
					echo '((<span class="km">km</span> * 1) * 1000) / <span class="stepdistance">steps</span> = ~<span class="steps">0</span> steps';
				echo '</div>';

				echo '<div id="sourcecode">';
					echo svgicon('git');
					echo '<a href="https://codeberg.org/airikr/calculate-steps" target="_blank" rel="noopener">Source code</a>';
				echo '</div>';
			?>

		</main>



	</body>
</html>