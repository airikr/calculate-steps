I have used numerous applications that measure one's steps over the years. All have shown wrong results.

Over 5 years ago, I used [Samsung Health](https://www.samsung.com/global/galaxy/apps/samsung-health/). A friend and I set up a contest to see who got to 10000 steps first. I went straight out and walked about 7500 steps. After being home for a few hours, just sitting in front of my computer with my phone on the table, I got a notification from the [Samsung Health](https://www.samsung.com/global/galaxy/apps/samsung-health/) app congratulating me on reaching 10000 steps.

I uninstalled the crap right away. Unreliable crap.

Today, I use an app called [Notify for Mi Band](https://mibandnotify.com/) to measure my sleep and heart rate via [Sleep as Android](https://play.google.com/store/apps/details?id=com.urbandroid.sleep). Since I use [Xiaomi Mi Band 5](https://www.mi.com/global/mi-smart-band-5/), I can't do more than use their native app for just [Sleep as Android](https://play.google.com/store/apps/details?id=com.urbandroid.sleep) (the app do not pick up Mi Band 5 with [Gadgetbridge](https://gadgetbridge.org/)). I usually take the activity band off when I wake up and put it back on when I go to sleep. During the day, I've seen that the notification from [Notify for Mi Band](https://mibandnotify.com/) shows that I've walked over 200 steps during that day.

Totally useless.

So I created a web page that calculates how many steps I take based on the number of kilometers I've walked. I trust mathematical calculations more than useless measurements from apps.

Keep in mind that the number of steps is only approximate as you may take longer or shorter steps than calculated.